# -*-coding:utf-8-*- 
# @Time : 2023/7/20 15:56
# @File : get_cookie.py

import time
import json

from selenium import webdriver

def get_cookie():
    bro = webdriver.Chrome()
    bro.get("https://wx.zsxq.com/")
    input("请用微信扫码登录>>>")
    cookie_data = bro.get_cookies()
    save_cookie(cookie_data)
    bro.quit()
    return "success"

def save_cookie(cookie_list):
    cookie = {}
    for c in cookie_list:
        cookie[c["name"]] = c['value']
    with open("cookie.txt", 'w', encoding='utf-8') as f1:
        f1.write(json.dumps(cookie))
        # data = f1.readlines()
        # print(json.loads(data[0]), type(json.loads(data[0])))
    return "save_cookie--success"

if __name__ == '__main__':
    get_cookie()
