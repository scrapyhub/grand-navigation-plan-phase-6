
import scrapy
from scrapy import Field



# 首页主页
class InitialPageItem(scrapy.Item):
    # 首页字段
    create_time = scrapy.Field()
    topic_id = scrapy.Field()
    group_info = scrapy.Field()
    content_type = scrapy.Field()
    content_create_time = scrapy.Field()
    content_modify_time = scrapy.Field()
    talk_owner = scrapy.Field()
    talk_text = scrapy.Field()
    talk_images = scrapy.Field()
    latest_likes = scrapy.Field()
    show_comments = scrapy.Field()
    annotation = scrapy.Field()
    likes_count = scrapy.Field()
    rewards_count = scrapy.Field()
    comments_count = scrapy.Field()
    digested = scrapy.Field()
    sticky = scrapy.Field()
    user_specific = scrapy.Field()
    hashtags = scrapy.Field()
    result_topic_id = scrapy.Field()
    result_group_id = scrapy.Field()
    result_digested = scrapy.Field()
    uid = scrapy.Field()
    objectId = scrapy.Field()
    className = scrapy.Field()


# 项目库
class ProjectRepositoryProjectItem(scrapy.Item):
    project_article_cnt = scrapy.Field()
    project_category_cnt = scrapy.Field()
    project_name = scrapy.Field()
    project_cnt = scrapy.Field()
    category_article_cnt = scrapy.Field()
    category_name = scrapy.Field()
    category_project_cnt = scrapy.Field()
    article_id = scrapy.Field()
    article_name = scrapy.Field()
    article_cnt = scrapy.Field()


# 项目库topic字典表
class ProjectRepositoryTopicItem(scrapy.Item):
    topic_label = scrapy.Field()
    topic_value = scrapy.Field()
    label = scrapy.Field()
    name = scrapy.Field()
    value = scrapy.Field()

# 项目库 article字典表
class ProjectRepositoryItem(scrapy.Item):
    content_count = scrapy.Field()
    hot = scrapy.Field()
    hot_words = scrapy.Field()
    toolbar = scrapy.Field()
    project = scrapy.Field()
    topic = scrapy.Field()


# 最新｜往期实战
class ActivityItem(scrapy.Item):
    id = scrapy.Field()
    gmt_create = scrapy.Field()
    name = scrapy.Field()
    label = scrapy.Field()
    cover_img = scrapy.Field()
    raw_name = scrapy.Field()

    template = scrapy.Field()
    template_raw_name = scrapy.Field()
    template_avatar = scrapy.Field()
    template_platform = scrapy.Field()
    template_target = scrapy.Field()
    template_refund_num = scrapy.Field()

    join_cnt = scrapy.Field()
    user_avatar = scrapy.Field()
    article_tag = scrapy.Field()
    gmt_start = scrapy.Field()
    gmt_0 = scrapy.Field()
    gmt_1 = scrapy.Field()
    gmt_end = scrapy.Field()
    price = scrapy.Field()
    menu = scrapy.Field()
    is_refund = scrapy.Field()


# 百问百答
class PracticalDetailsDiscussItem(scrapy.Item):
    objectId = scrapy.Field()
    tag = scrapy.Field()
    extra = scrapy.Field()
    qid = scrapy.Field()
    gmt_create = scrapy.Field()
    category = scrapy.Field()
    title = scrapy.Field()
    href = scrapy.Field()
    content = scrapy.Field()
    activity_id = scrapy.Field()
    createdAt = scrapy.Field()
    updatedAt = scrapy.Field()
    like_list = scrapy.Field()
    elastic = scrapy.Field()
    ACL = scrapy.Field()

# 高手分享
class PracticalDetailsVeteranItem(scrapy.Item):
    id = scrapy.Field()
    gmt_create = scrapy.Field()
    activity_id = scrapy.Field()
    activity_name = scrapy.Field()
    activity_label = scrapy.Field()
    category = scrapy.Field()
    title = scrapy.Field()
    tag = scrapy.Field()
    href = scrapy.Field()
    content = scrapy.Field()
    extra = scrapy.Field()
    is_delete = scrapy.Field()

# 项目库详情页
class ProjectRepositoryDetailsItem(scrapy.Item):
    menu_id = scrapy.Field()
    article_content = scrapy.Field()
    comments_count = scrapy.Field()
    create_user_id = scrapy.Field()
    create_user_level = scrapy.Field()
    show_create_user_id = scrapy.Field()
    show_title = scrapy.Field()
    gmt_create = scrapy.Field()
    gmt_update = scrapy.Field()
    is_digested = scrapy.Field()
    like_count = scrapy.Field()
    rewards_count = scrapy.Field()
    reading_count = scrapy.Field()
    menu_ids = scrapy.Field()
    type = scrapy.Field()
    task_topic_id = scrapy.Field()
    topic_id = scrapy.Field()
    menu_info = scrapy.Field()
    user_info = scrapy.Field()


