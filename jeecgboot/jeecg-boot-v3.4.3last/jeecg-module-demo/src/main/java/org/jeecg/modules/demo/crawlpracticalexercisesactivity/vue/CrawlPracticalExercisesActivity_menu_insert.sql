-- 注意：该页面对应的前台目录为views/crawlpracticalexercisesactivity文件夹下
-- 如果你想更改到其他目录，请修改sql中component字段对应的值


INSERT INTO sys_permission(id, parent_id, name, url, component, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_route, is_leaf, keep_alive, hidden, hide_tab, description, status, del_flag, rule_flag, create_by, create_time, update_by, update_time, internal_or_external) 
VALUES ('2023080704043750120', NULL, '往期实战', '/crawlpracticalexercisesactivity/crawlPracticalExercisesActivityList', 'crawlpracticalexercisesactivity/CrawlPracticalExercisesActivityList', NULL, NULL, 0, NULL, '1', 0.00, 0, NULL, 1, 1, 0, 0, 0, NULL, '1', 0, 0, 'admin', '2023-08-07 16:04:12', NULL, NULL, 0);

-- 权限控制sql
-- 新增
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2023080704043750121', '2023080704043750120', '添加往期实战', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:crawl_practical_exercises_activity:add', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2023-08-07 16:04:12', NULL, NULL, 0, 0, '1', 0);
-- 编辑
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2023080704043750122', '2023080704043750120', '编辑往期实战', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:crawl_practical_exercises_activity:edit', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2023-08-07 16:04:12', NULL, NULL, 0, 0, '1', 0);
-- 删除
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2023080704043750123', '2023080704043750120', '删除往期实战', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:crawl_practical_exercises_activity:delete', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2023-08-07 16:04:12', NULL, NULL, 0, 0, '1', 0);
-- 批量删除
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2023080704043750124', '2023080704043750120', '批量删除往期实战', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:crawl_practical_exercises_activity:deleteBatch', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2023-08-07 16:04:12', NULL, NULL, 0, 0, '1', 0);
-- 导出excel
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2023080704043750125', '2023080704043750120', '导出excel_往期实战', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:crawl_practical_exercises_activity:exportXls', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2023-08-07 16:04:12', NULL, NULL, 0, 0, '1', 0);
-- 导入excel
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2023080704043750126', '2023080704043750120', '导入excel_往期实战', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:crawl_practical_exercises_activity:importExcel', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2023-08-07 16:04:12', NULL, NULL, 0, 0, '1', 0);