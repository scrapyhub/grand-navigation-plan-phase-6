package org.jeecg.modules.demo.crawlpracticalexercisesactivity.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.crawlpracticalexercisesactivity.entity.CrawlPracticalExercisesActivity;
import org.jeecg.modules.demo.crawlpracticalexercisesactivity.service.ICrawlPracticalExercisesActivityService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 往期实战
 * @Author: jeecg-boot
 * @Date:   2023-08-07
 * @Version: V1.0
 */
@Api(tags="往期实战")
@RestController
@RequestMapping("/crawlpracticalexercisesactivity/crawlPracticalExercisesActivity")
@Slf4j
public class CrawlPracticalExercisesActivityController extends JeecgController<CrawlPracticalExercisesActivity, ICrawlPracticalExercisesActivityService> {
	@Autowired
	private ICrawlPracticalExercisesActivityService crawlPracticalExercisesActivityService;
	
	/**
	 * 分页列表查询
	 *
	 * @param crawlPracticalExercisesActivity
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "往期实战-分页列表查询")
	@ApiOperation(value="往期实战-分页列表查询", notes="往期实战-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<CrawlPracticalExercisesActivity>> queryPageList(CrawlPracticalExercisesActivity crawlPracticalExercisesActivity,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<CrawlPracticalExercisesActivity> queryWrapper = QueryGenerator.initQueryWrapper(crawlPracticalExercisesActivity, req.getParameterMap());
		Page<CrawlPracticalExercisesActivity> page = new Page<CrawlPracticalExercisesActivity>(pageNo, pageSize);
		IPage<CrawlPracticalExercisesActivity> pageList = crawlPracticalExercisesActivityService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param crawlPracticalExercisesActivity
	 * @return
	 */
	@AutoLog(value = "往期实战-添加")
	@ApiOperation(value="往期实战-添加", notes="往期实战-添加")
	//@RequiresPermissions("org.jeecg.modules.demo:crawl_practical_exercises_activity:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody CrawlPracticalExercisesActivity crawlPracticalExercisesActivity) {
		crawlPracticalExercisesActivityService.save(crawlPracticalExercisesActivity);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param crawlPracticalExercisesActivity
	 * @return
	 */
	@AutoLog(value = "往期实战-编辑")
	@ApiOperation(value="往期实战-编辑", notes="往期实战-编辑")
	//@RequiresPermissions("org.jeecg.modules.demo:crawl_practical_exercises_activity:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody CrawlPracticalExercisesActivity crawlPracticalExercisesActivity) {
		crawlPracticalExercisesActivityService.updateById(crawlPracticalExercisesActivity);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "往期实战-通过id删除")
	@ApiOperation(value="往期实战-通过id删除", notes="往期实战-通过id删除")
	//@RequiresPermissions("org.jeecg.modules.demo:crawl_practical_exercises_activity:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		crawlPracticalExercisesActivityService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "往期实战-批量删除")
	@ApiOperation(value="往期实战-批量删除", notes="往期实战-批量删除")
	//@RequiresPermissions("org.jeecg.modules.demo:crawl_practical_exercises_activity:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.crawlPracticalExercisesActivityService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "往期实战-通过id查询")
	@ApiOperation(value="往期实战-通过id查询", notes="往期实战-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<CrawlPracticalExercisesActivity> queryById(@RequestParam(name="id",required=true) String id) {
		CrawlPracticalExercisesActivity crawlPracticalExercisesActivity = crawlPracticalExercisesActivityService.getById(id);
		if(crawlPracticalExercisesActivity==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(crawlPracticalExercisesActivity);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param crawlPracticalExercisesActivity
    */
    //@RequiresPermissions("org.jeecg.modules.demo:crawl_practical_exercises_activity:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CrawlPracticalExercisesActivity crawlPracticalExercisesActivity) {
        return super.exportXls(request, crawlPracticalExercisesActivity, CrawlPracticalExercisesActivity.class, "往期实战");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    //@RequiresPermissions("crawl_practical_exercises_activity:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, CrawlPracticalExercisesActivity.class);
    }

}
