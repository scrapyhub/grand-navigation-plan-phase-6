package org.jeecg.modules.demo.crawlpracticaldetailsdiscuss.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 百问百答
 * @Author: jeecg-boot
 * @Date:   2023-08-08
 * @Version: V1.0
 */
@Data
@TableName("crawl_practical_details_discuss")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="crawl_practical_details_discuss对象", description="百问百答")
public class CrawlPracticalDetailsDiscuss implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private java.lang.Integer id;
	/**对象ID*/
	@Excel(name = "对象ID", width = 15)
    @ApiModelProperty(value = "对象ID")
    private java.lang.String objectid;
	/**标签*/
	@Excel(name = "标签", width = 15)
    @ApiModelProperty(value = "标签")
    private java.lang.String tag;
	/**标签*/
	@Excel(name = "标签", width = 15)
    @ApiModelProperty(value = "标签")
    private java.lang.String extra;
	/**问题ID*/
	@Excel(name = "问题ID", width = 15)
    @ApiModelProperty(value = "问题ID")
    private java.lang.Integer qid;
	/**创建时间*/
	@Excel(name = "创建时间", width = 15)
    @ApiModelProperty(value = "创建时间")
    private java.lang.Integer gmtCreate;
	/**分类*/
	@Excel(name = "分类", width = 15)
    @ApiModelProperty(value = "分类")
    private java.lang.String category;
	/**标题*/
	@Excel(name = "标题", width = 15)
    @ApiModelProperty(value = "标题")
    private java.lang.String title;
	/**链接*/
	@Excel(name = "链接", width = 15)
    @ApiModelProperty(value = "链接")
    private java.lang.String href;
	/**内容*/
	@Excel(name = "内容", width = 15)
    @ApiModelProperty(value = "内容")
    private java.lang.String content;
	/**活动ID*/
	@Excel(name = "活动ID", width = 15)
    @ApiModelProperty(value = "活动ID")
    private java.lang.Integer activityId;
	/**创建时间*/
	@Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createdat;
	/**更新时间*/
	@Excel(name = "更新时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updatedat;
	/**likeList*/
	@Excel(name = "likeList", width = 15)
    @ApiModelProperty(value = "likeList")
    private java.lang.String likeList;
	/**elastic*/
	@Excel(name = "elastic", width = 15)
    @ApiModelProperty(value = "elastic")
    private java.lang.Integer elastic;
	/**访问控制列表*/
	@Excel(name = "访问控制列表", width = 15)
    @ApiModelProperty(value = "访问控制列表")
    private java.lang.String acl;
}
