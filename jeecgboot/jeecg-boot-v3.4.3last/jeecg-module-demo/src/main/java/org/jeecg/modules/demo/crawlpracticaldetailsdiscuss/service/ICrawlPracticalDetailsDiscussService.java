package org.jeecg.modules.demo.crawlpracticaldetailsdiscuss.service;

import org.jeecg.modules.demo.crawlpracticaldetailsdiscuss.entity.CrawlPracticalDetailsDiscuss;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 百问百答
 * @Author: jeecg-boot
 * @Date:   2023-08-08
 * @Version: V1.0
 */
public interface ICrawlPracticalDetailsDiscussService extends IService<CrawlPracticalDetailsDiscuss> {

}
