package org.jeecg.modules.demo.crawlpracticalexercisesactivity.service;

import org.jeecg.modules.demo.crawlpracticalexercisesactivity.entity.CrawlPracticalExercisesActivity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 往期实战
 * @Author: jeecg-boot
 * @Date:   2023-08-07
 * @Version: V1.0
 */
public interface ICrawlPracticalExercisesActivityService extends IService<CrawlPracticalExercisesActivity> {

}
