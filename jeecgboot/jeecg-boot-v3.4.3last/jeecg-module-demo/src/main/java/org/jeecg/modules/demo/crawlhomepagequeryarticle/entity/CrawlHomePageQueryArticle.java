package org.jeecg.modules.demo.crawlhomepagequeryarticle.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 最新主题列表
 * @Author: jeecg-boot
 * @Date:   2023-09-02
 * @Version: V1.0
 */
@Data
@TableName("crawl_home_page_query_article")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="帖子主题列表", description="最新主题列表")
public class CrawlHomePageQueryArticle implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private java.lang.Integer id;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
	/**主题ID*/
	@Excel(name = "主题ID", width = 15)
    @ApiModelProperty(value = "主题ID")
    private java.lang.String topicId;
	/**组ID*/
	@Excel(name = "组ID", width = 15)
    @ApiModelProperty(value = "组ID")
    private java.lang.String groupInfo;
	/**内容类型*/
	@Excel(name = "内容类型", width = 15)
    @ApiModelProperty(value = "内容类型")
    private java.lang.String contentType;
	/**发布者信息*/
	@Excel(name = "发布者信息", width = 15)
    @ApiModelProperty(value = "发布者信息")
    private java.lang.String talkOwner;
	/**文本内容*/
	@Excel(name = "文本内容", width = 15)
    @ApiModelProperty(value = "文本内容")
    private java.lang.String talkText;
	/**图片内容*/
	@Excel(name = "图片内容", width = 15)
    @ApiModelProperty(value = "图片内容")
    private java.lang.String talkImages;
	/**最新点赞数*/
	@Excel(name = "最新点赞数", width = 15)
    @ApiModelProperty(value = "最新点赞数")
    private java.lang.String latestLikes;
	/**显示评论*/
	@Excel(name = "显示评论", width = 15)
    @ApiModelProperty(value = "显示评论")
    private java.lang.String showComments;
	/**点赞数*/
	@Excel(name = "点赞数", width = 15)
    @ApiModelProperty(value = "点赞数")
    private java.lang.Integer likesCount;
	/**赞赏数*/
	@Excel(name = "赞赏数", width = 15)
    @ApiModelProperty(value = "赞赏数")
    private java.lang.Integer rewardsCount;
	/**评论数*/
	@Excel(name = "评论数", width = 15)
    @ApiModelProperty(value = "评论数")
    private java.lang.Integer commentsCount;
	/**是否精华*/
	@Excel(name = "是否精华", width = 15)
    @ApiModelProperty(value = "是否精华")
    private java.lang.Integer digested;
	/**是否置顶*/
	@Excel(name = "是否置顶", width = 15)
    @ApiModelProperty(value = "是否置顶")
    private java.lang.Integer sticky;
	/**用户特定信息*/
	@Excel(name = "用户特定信息", width = 15)
    @ApiModelProperty(value = "用户特定信息")
    private java.lang.String userSpecific;
	/**标签*/
	@Excel(name = "标签", width = 15)
    @ApiModelProperty(value = "标签")
    private java.lang.String hashtags;
	/**用户ID*/
	@Excel(name = "用户ID", width = 15)
    @ApiModelProperty(value = "用户ID")
    private java.lang.String uid;
	/**对象类型*/
	@Excel(name = "对象类型", width = 15)
    @ApiModelProperty(value = "对象类型")
    private java.lang.String classname;
	/**阅读数量*/
	@Excel(name = "阅读数量", width = 15)
    @ApiModelProperty(value = "阅读数量")
    private java.lang.Integer readingCount;
	/**阅读人数*/
	@Excel(name = "阅读人数", width = 15)
    @ApiModelProperty(value = "阅读人数")
    private java.lang.Integer readersCount;

    /**头像*/
    @Excel(name = "发布者头像", width = 15)
    @ApiModelProperty(value = "发布者头像")
    private java.lang.String userHeadimg;

	/**附件列表*/
	@Excel(name = "附件列表", width = 15)
    @ApiModelProperty(value = "附件列表")
    private java.lang.String talkFiles;
}
