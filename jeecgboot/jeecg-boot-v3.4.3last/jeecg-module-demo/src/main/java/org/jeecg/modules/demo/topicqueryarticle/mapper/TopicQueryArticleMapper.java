package org.jeecg.modules.demo.topicqueryarticle.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.demo.topicqueryarticle.entity.TopicQueryArticle;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 主题详情页
 * @Author: jeecg-boot
 * @Date:   2023-08-03
 * @Version: V1.0
 */
public interface TopicQueryArticleMapper extends BaseMapper<TopicQueryArticle> {

}
