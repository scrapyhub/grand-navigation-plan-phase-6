package org.jeecg.modules.demo.crawlpracticaldetailsdiscuss.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.demo.crawlpracticaldetailsdiscuss.entity.CrawlPracticalDetailsDiscuss;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 百问百答
 * @Author: jeecg-boot
 * @Date:   2023-08-08
 * @Version: V1.0
 */
public interface CrawlPracticalDetailsDiscussMapper extends BaseMapper<CrawlPracticalDetailsDiscuss> {

}
