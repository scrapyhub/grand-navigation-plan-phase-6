package org.jeecg.modules.demo.crawlpracticaldetailsVeteran.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.demo.crawlpracticaldetailsVeteran.entity.CrawlPracticalDetailsVeteran;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 高手分享
 * @Author: jeecg-boot
 * @Date:   2023-08-07
 * @Version: V1.0
 */
public interface CrawlPracticalDetailsVeteranMapper extends BaseMapper<CrawlPracticalDetailsVeteran> {

}
