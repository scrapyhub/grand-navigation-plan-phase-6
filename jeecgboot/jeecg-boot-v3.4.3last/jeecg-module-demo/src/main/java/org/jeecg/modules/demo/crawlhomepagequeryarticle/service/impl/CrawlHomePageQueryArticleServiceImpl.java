package org.jeecg.modules.demo.crawlhomepagequeryarticle.service.impl;

import org.jeecg.modules.demo.crawlhomepagequeryarticle.entity.CrawlHomePageQueryArticle;
import org.jeecg.modules.demo.crawlhomepagequeryarticle.mapper.CrawlHomePageQueryArticleMapper;
import org.jeecg.modules.demo.crawlhomepagequeryarticle.service.ICrawlHomePageQueryArticleService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 最新主题列表
 * @Author: jeecg-boot
 * @Date:   2023-09-02
 * @Version: V1.0
 */
@Service
public class CrawlHomePageQueryArticleServiceImpl extends ServiceImpl<CrawlHomePageQueryArticleMapper, CrawlHomePageQueryArticle> implements ICrawlHomePageQueryArticleService {

}
