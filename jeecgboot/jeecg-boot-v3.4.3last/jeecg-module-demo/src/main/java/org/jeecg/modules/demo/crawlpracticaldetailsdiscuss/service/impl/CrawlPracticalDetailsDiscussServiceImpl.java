package org.jeecg.modules.demo.crawlpracticaldetailsdiscuss.service.impl;

import org.jeecg.modules.demo.crawlpracticaldetailsdiscuss.entity.CrawlPracticalDetailsDiscuss;
import org.jeecg.modules.demo.crawlpracticaldetailsdiscuss.mapper.CrawlPracticalDetailsDiscussMapper;
import org.jeecg.modules.demo.crawlpracticaldetailsdiscuss.service.ICrawlPracticalDetailsDiscussService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 百问百答
 * @Author: jeecg-boot
 * @Date:   2023-08-08
 * @Version: V1.0
 */
@Service
public class CrawlPracticalDetailsDiscussServiceImpl extends ServiceImpl<CrawlPracticalDetailsDiscussMapper, CrawlPracticalDetailsDiscuss> implements ICrawlPracticalDetailsDiscussService {

}
