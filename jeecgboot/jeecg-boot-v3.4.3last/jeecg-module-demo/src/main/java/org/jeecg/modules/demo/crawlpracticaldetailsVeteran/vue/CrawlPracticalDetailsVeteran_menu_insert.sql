-- 注意：该页面对应的前台目录为views/crawlpracticaldetailsVeteran文件夹下
-- 如果你想更改到其他目录，请修改sql中component字段对应的值


INSERT INTO sys_permission(id, parent_id, name, url, component, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_route, is_leaf, keep_alive, hidden, hide_tab, description, status, del_flag, rule_flag, create_by, create_time, update_by, update_time, internal_or_external) 
VALUES ('2023080704365900410', NULL, '高手分享', '/crawlpracticaldetailsVeteran/crawlPracticalDetailsVeteranList', 'crawlpracticaldetailsVeteran/CrawlPracticalDetailsVeteranList', NULL, NULL, 0, NULL, '1', 0.00, 0, NULL, 1, 1, 0, 0, 0, NULL, '1', 0, 0, 'admin', '2023-08-07 16:36:41', NULL, NULL, 0);

-- 权限控制sql
-- 新增
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2023080704365900411', '2023080704365900410', '添加高手分享', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:crawl_practical_details_veteran:add', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2023-08-07 16:36:41', NULL, NULL, 0, 0, '1', 0);
-- 编辑
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2023080704365900412', '2023080704365900410', '编辑高手分享', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:crawl_practical_details_veteran:edit', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2023-08-07 16:36:41', NULL, NULL, 0, 0, '1', 0);
-- 删除
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2023080704365900413', '2023080704365900410', '删除高手分享', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:crawl_practical_details_veteran:delete', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2023-08-07 16:36:41', NULL, NULL, 0, 0, '1', 0);
-- 批量删除
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2023080704365900414', '2023080704365900410', '批量删除高手分享', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:crawl_practical_details_veteran:deleteBatch', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2023-08-07 16:36:41', NULL, NULL, 0, 0, '1', 0);
-- 导出excel
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2023080704365900415', '2023080704365900410', '导出excel_高手分享', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:crawl_practical_details_veteran:exportXls', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2023-08-07 16:36:41', NULL, NULL, 0, 0, '1', 0);
-- 导入excel
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2023080704365900416', '2023080704365900410', '导入excel_高手分享', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:crawl_practical_details_veteran:importExcel', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2023-08-07 16:36:41', NULL, NULL, 0, 0, '1', 0);