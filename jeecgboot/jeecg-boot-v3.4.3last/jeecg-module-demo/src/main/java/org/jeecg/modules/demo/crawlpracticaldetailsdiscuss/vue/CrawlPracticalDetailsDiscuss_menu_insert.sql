-- 注意：该页面对应的前台目录为views/crawlpracticaldetailsdiscuss文件夹下
-- 如果你想更改到其他目录，请修改sql中component字段对应的值


INSERT INTO sys_permission(id, parent_id, name, url, component, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_route, is_leaf, keep_alive, hidden, hide_tab, description, status, del_flag, rule_flag, create_by, create_time, update_by, update_time, internal_or_external) 
VALUES ('2023080810127470570', NULL, '百问百答', '/crawlpracticaldetailsdiscuss/crawlPracticalDetailsDiscussList', 'crawlpracticaldetailsdiscuss/CrawlPracticalDetailsDiscussList', NULL, NULL, 0, NULL, '1', 0.00, 0, NULL, 1, 1, 0, 0, 0, NULL, '1', 0, 0, 'admin', '2023-08-08 10:12:57', NULL, NULL, 0);

-- 权限控制sql
-- 新增
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2023080810127470571', '2023080810127470570', '添加百问百答', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:crawl_practical_details_discuss:add', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2023-08-08 10:12:57', NULL, NULL, 0, 0, '1', 0);
-- 编辑
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2023080810127470572', '2023080810127470570', '编辑百问百答', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:crawl_practical_details_discuss:edit', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2023-08-08 10:12:57', NULL, NULL, 0, 0, '1', 0);
-- 删除
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2023080810127470573', '2023080810127470570', '删除百问百答', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:crawl_practical_details_discuss:delete', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2023-08-08 10:12:57', NULL, NULL, 0, 0, '1', 0);
-- 批量删除
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2023080810127470574', '2023080810127470570', '批量删除百问百答', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:crawl_practical_details_discuss:deleteBatch', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2023-08-08 10:12:57', NULL, NULL, 0, 0, '1', 0);
-- 导出excel
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2023080810127470575', '2023080810127470570', '导出excel_百问百答', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:crawl_practical_details_discuss:exportXls', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2023-08-08 10:12:57', NULL, NULL, 0, 0, '1', 0);
-- 导入excel
INSERT INTO sys_permission(id, parent_id, name, url, component, is_route, component_name, redirect, menu_type, perms, perms_type, sort_no, always_show, icon, is_leaf, keep_alive, hidden, hide_tab, description, create_by, create_time, update_by, update_time, del_flag, rule_flag, status, internal_or_external)
VALUES ('2023080810127470576', '2023080810127470570', '导入excel_百问百答', NULL, NULL, 0, NULL, NULL, 2, 'org.jeecg.modules.demo:crawl_practical_details_discuss:importExcel', '1', NULL, 0, NULL, 1, 0, 0, 0, NULL, 'admin', '2023-08-08 10:12:57', NULL, NULL, 0, 0, '1', 0);