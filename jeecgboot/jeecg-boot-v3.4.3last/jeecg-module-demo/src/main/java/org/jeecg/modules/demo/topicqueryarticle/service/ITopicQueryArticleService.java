package org.jeecg.modules.demo.topicqueryarticle.service;

import org.jeecg.modules.demo.test.entity.JeecgOrderTicket;
import org.jeecg.modules.demo.topicqueryarticle.entity.TopicQueryArticle;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description: 主题详情页
 * @Author: jeecg-boot
 * @Date:   2023-08-03
 * @Version: V1.0
 */
public interface ITopicQueryArticleService extends IService<TopicQueryArticle> {

    TopicQueryArticle getBytopicId(String topicId);
}
