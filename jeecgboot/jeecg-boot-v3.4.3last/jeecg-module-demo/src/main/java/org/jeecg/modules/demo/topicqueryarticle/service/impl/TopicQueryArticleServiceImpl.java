package org.jeecg.modules.demo.topicqueryarticle.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.jeecg.modules.demo.topicqueryarticle.entity.TopicQueryArticle;
import org.jeecg.modules.demo.topicqueryarticle.mapper.TopicQueryArticleMapper;
import org.jeecg.modules.demo.topicqueryarticle.service.ITopicQueryArticleService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description: 主题详情页
 * @Author: jeecg-boot
 * @Date:   2023-08-03
 * @Version: V1.0
 */
@Service
public class TopicQueryArticleServiceImpl extends ServiceImpl<TopicQueryArticleMapper, TopicQueryArticle> implements ITopicQueryArticleService {

    public TopicQueryArticle getBytopicId(String topicId) {
        QueryWrapper<TopicQueryArticle> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(TopicQueryArticle::getTopicId,topicId);
//
//        select * from crawl_topic_query_article where topic_id = topicId
//
        return this.getOne(queryWrapper);
    }
}
