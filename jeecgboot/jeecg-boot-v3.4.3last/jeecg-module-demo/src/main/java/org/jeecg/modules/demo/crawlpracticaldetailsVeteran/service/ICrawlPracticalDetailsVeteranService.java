package org.jeecg.modules.demo.crawlpracticaldetailsVeteran.service;

import org.jeecg.modules.demo.crawlpracticaldetailsVeteran.entity.CrawlPracticalDetailsVeteran;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 高手分享
 * @Author: jeecg-boot
 * @Date:   2023-08-07
 * @Version: V1.0
 */
public interface ICrawlPracticalDetailsVeteranService extends IService<CrawlPracticalDetailsVeteran> {

}
