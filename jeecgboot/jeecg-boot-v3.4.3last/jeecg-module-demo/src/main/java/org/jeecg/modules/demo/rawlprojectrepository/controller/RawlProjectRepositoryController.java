package org.jeecg.modules.demo.rawlprojectrepository.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.rawlprojectrepository.entity.RawlProjectRepository;
import org.jeecg.modules.demo.rawlprojectrepository.service.IRawlProjectRepositoryService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 项目库列表
 * @Author: jeecg-boot
 * @Date:   2023-08-12
 * @Version: V1.0
 */
@Api(tags="项目库列表")
@RestController
@RequestMapping("/rawlprojectrepository/rawlProjectRepository")
@Slf4j
public class RawlProjectRepositoryController extends JeecgController<RawlProjectRepository, IRawlProjectRepositoryService> {
	@Autowired
	private IRawlProjectRepositoryService rawlProjectRepositoryService;
	
	/**
	 * 分页列表查询
	 *
	 * @param rawlProjectRepository
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "项目库列表-分页列表查询")
	@ApiOperation(value="项目库列表-分页列表查询", notes="项目库列表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<RawlProjectRepository>> queryPageList(RawlProjectRepository rawlProjectRepository,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<RawlProjectRepository> queryWrapper = QueryGenerator.initQueryWrapper(rawlProjectRepository, req.getParameterMap());
		Page<RawlProjectRepository> page = new Page<RawlProjectRepository>(pageNo, pageSize);
		IPage<RawlProjectRepository> pageList = rawlProjectRepositoryService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param rawlProjectRepository
	 * @return
	 */
	@AutoLog(value = "项目库列表-添加")
	@ApiOperation(value="项目库列表-添加", notes="项目库列表-添加")
	//@RequiresPermissions("org.jeecg.modules.demo:rawl_project_repository:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody RawlProjectRepository rawlProjectRepository) {
		rawlProjectRepositoryService.save(rawlProjectRepository);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param rawlProjectRepository
	 * @return
	 */
	@AutoLog(value = "项目库列表-编辑")
	@ApiOperation(value="项目库列表-编辑", notes="项目库列表-编辑")
	//@RequiresPermissions("org.jeecg.modules.demo:rawl_project_repository:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody RawlProjectRepository rawlProjectRepository) {
		rawlProjectRepositoryService.updateById(rawlProjectRepository);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "项目库列表-通过id删除")
	@ApiOperation(value="项目库列表-通过id删除", notes="项目库列表-通过id删除")
	//@RequiresPermissions("org.jeecg.modules.demo:rawl_project_repository:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		rawlProjectRepositoryService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "项目库列表-批量删除")
	@ApiOperation(value="项目库列表-批量删除", notes="项目库列表-批量删除")
	//@RequiresPermissions("org.jeecg.modules.demo:rawl_project_repository:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.rawlProjectRepositoryService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "项目库列表-通过id查询")
	@ApiOperation(value="项目库列表-通过id查询", notes="项目库列表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<RawlProjectRepository> queryById(@RequestParam(name="id",required=true) String id) {
		RawlProjectRepository rawlProjectRepository = rawlProjectRepositoryService.getById(id);
		if(rawlProjectRepository==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(rawlProjectRepository);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param rawlProjectRepository
    */
    //@RequiresPermissions("org.jeecg.modules.demo:rawl_project_repository:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, RawlProjectRepository rawlProjectRepository) {
        return super.exportXls(request, rawlProjectRepository, RawlProjectRepository.class, "项目库列表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    //@RequiresPermissions("rawl_project_repository:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, RawlProjectRepository.class);
    }

}
