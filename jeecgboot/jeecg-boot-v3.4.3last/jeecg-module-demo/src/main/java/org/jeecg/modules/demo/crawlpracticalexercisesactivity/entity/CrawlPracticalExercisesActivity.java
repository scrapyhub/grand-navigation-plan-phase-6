package org.jeecg.modules.demo.crawlpracticalexercisesactivity.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 往期实战
 * @Author: jeecg-boot
 * @Date:   2023-08-07
 * @Version: V1.0
 */
@Data
@TableName("crawl_practical_exercises_activity")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="crawl_practical_exercises_activity对象", description="往期实战")
public class CrawlPracticalExercisesActivity implements Serializable {
    private static final long serialVersionUID = 1L;

	/**活动id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "活动id")
    private java.lang.Integer id;
	/**创建时间*/
	@Excel(name = "创建时间", width = 15)
    @ApiModelProperty(value = "创建时间")
    private java.lang.Integer gmtCreate;
	/**活动名称*/
	@Excel(name = "活动名称", width = 15)
    @ApiModelProperty(value = "活动名称")
    private java.lang.String name;
	/**活动标签*/
	@Excel(name = "活动标签", width = 15)
    @ApiModelProperty(value = "活动标签")
    private java.lang.String activitytag;
	/**封面图片*/
	@Excel(name = "封面图片", width = 15)
    @ApiModelProperty(value = "封面图片")
    private java.lang.String coverImg;
	/**原始名称*/
	@Excel(name = "原始名称", width = 15)
    @ApiModelProperty(value = "原始名称")
    private java.lang.String rawName;
	/**模板*/
	@Excel(name = "模板", width = 15)
    @ApiModelProperty(value = "模板")
    private java.lang.String template;
	/**模板原始名称*/
	@Excel(name = "模板原始名称", width = 15)
    @ApiModelProperty(value = "模板原始名称")
    private java.lang.String templateRawName;
	/**模板头像*/
	@Excel(name = "模板头像", width = 15)
    @ApiModelProperty(value = "模板头像")
    private java.lang.String templateAvatar;
	/**模板平台*/
	@Excel(name = "模板平台", width = 15)
    @ApiModelProperty(value = "模板平台")
    private java.lang.String templatePlatform;
	/**模板目标*/
	@Excel(name = "模板目标", width = 15)
    @ApiModelProperty(value = "模板目标")
    private java.lang.String templateTarget;
	/**模板退款次数*/
	@Excel(name = "模板退款次数", width = 15)
    @ApiModelProperty(value = "模板退款次数")
    private java.lang.Integer templateRefundNum;
	/**参与人数*/
	@Excel(name = "参与人数", width = 15)
    @ApiModelProperty(value = "参与人数")
    private java.lang.Integer joinCnt;
	/**用户头像*/
	@Excel(name = "用户头像", width = 15)
    @ApiModelProperty(value = "用户头像")
    private java.lang.String userAvatar;
	/**文章标签*/
	@Excel(name = "文章标签", width = 15)
    @ApiModelProperty(value = "文章标签")
    private java.lang.String articleTag;
	/**开始时间*/
	@Excel(name = "开始时间", width = 15)
    @ApiModelProperty(value = "开始时间")
    private java.lang.Integer gmtStart;
	/**时间点0*/
	@Excel(name = "时间点0", width = 15)
    @ApiModelProperty(value = "时间点0")
    private java.lang.Integer gmtStart0;
	/**时间点1*/
	@Excel(name = "时间点1", width = 15)
    @ApiModelProperty(value = "时间点1")
    private java.lang.Integer gmtEnt1;
	/**结束时间*/
	@Excel(name = "结束时间", width = 15)
    @ApiModelProperty(value = "结束时间")
    private java.lang.Integer gmtEnd;
	/**价格*/
	@Excel(name = "价格", width = 15)
    @ApiModelProperty(value = "价格")
    private java.lang.Integer price;
	/**菜单*/
	@Excel(name = "菜单", width = 15)
    @ApiModelProperty(value = "菜单")
    private java.lang.String menu;
	/**是否退款*/
	@Excel(name = "是否退款", width = 15)
    @ApiModelProperty(value = "是否退款")
    private java.lang.Integer isRefund;
}
