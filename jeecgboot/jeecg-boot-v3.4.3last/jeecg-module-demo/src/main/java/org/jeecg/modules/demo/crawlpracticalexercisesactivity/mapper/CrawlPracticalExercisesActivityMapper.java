package org.jeecg.modules.demo.crawlpracticalexercisesactivity.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.demo.crawlpracticalexercisesactivity.entity.CrawlPracticalExercisesActivity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 往期实战
 * @Author: jeecg-boot
 * @Date:   2023-08-07
 * @Version: V1.0
 */
public interface CrawlPracticalExercisesActivityMapper extends BaseMapper<CrawlPracticalExercisesActivity> {

}
