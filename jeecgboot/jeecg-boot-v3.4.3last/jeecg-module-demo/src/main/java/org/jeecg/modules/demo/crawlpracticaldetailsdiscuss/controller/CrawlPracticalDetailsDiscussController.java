package org.jeecg.modules.demo.crawlpracticaldetailsdiscuss.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.crawlpracticaldetailsdiscuss.entity.CrawlPracticalDetailsDiscuss;
import org.jeecg.modules.demo.crawlpracticaldetailsdiscuss.service.ICrawlPracticalDetailsDiscussService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 百问百答
 * @Author: jeecg-boot
 * @Date:   2023-08-08
 * @Version: V1.0
 */
@Api(tags="百问百答")
@RestController
@RequestMapping("/crawlpracticaldetailsdiscuss/crawlPracticalDetailsDiscuss")
@Slf4j
public class CrawlPracticalDetailsDiscussController extends JeecgController<CrawlPracticalDetailsDiscuss, ICrawlPracticalDetailsDiscussService> {
	@Autowired
	private ICrawlPracticalDetailsDiscussService crawlPracticalDetailsDiscussService;
	
	/**
	 * 分页列表查询
	 *
	 * @param crawlPracticalDetailsDiscuss
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "百问百答-分页列表查询")
	@ApiOperation(value="百问百答-分页列表查询", notes="百问百答-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<CrawlPracticalDetailsDiscuss>> queryPageList(CrawlPracticalDetailsDiscuss crawlPracticalDetailsDiscuss,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<CrawlPracticalDetailsDiscuss> queryWrapper = QueryGenerator.initQueryWrapper(crawlPracticalDetailsDiscuss, req.getParameterMap());
		Page<CrawlPracticalDetailsDiscuss> page = new Page<CrawlPracticalDetailsDiscuss>(pageNo, pageSize);
		IPage<CrawlPracticalDetailsDiscuss> pageList = crawlPracticalDetailsDiscussService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param crawlPracticalDetailsDiscuss
	 * @return
	 */
	@AutoLog(value = "百问百答-添加")
	@ApiOperation(value="百问百答-添加", notes="百问百答-添加")
	//@RequiresPermissions("org.jeecg.modules.demo:crawl_practical_details_discuss:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody CrawlPracticalDetailsDiscuss crawlPracticalDetailsDiscuss) {
		crawlPracticalDetailsDiscussService.save(crawlPracticalDetailsDiscuss);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param crawlPracticalDetailsDiscuss
	 * @return
	 */
	@AutoLog(value = "百问百答-编辑")
	@ApiOperation(value="百问百答-编辑", notes="百问百答-编辑")
	//@RequiresPermissions("org.jeecg.modules.demo:crawl_practical_details_discuss:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody CrawlPracticalDetailsDiscuss crawlPracticalDetailsDiscuss) {
		crawlPracticalDetailsDiscussService.updateById(crawlPracticalDetailsDiscuss);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "百问百答-通过id删除")
	@ApiOperation(value="百问百答-通过id删除", notes="百问百答-通过id删除")
	//@RequiresPermissions("org.jeecg.modules.demo:crawl_practical_details_discuss:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		crawlPracticalDetailsDiscussService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "百问百答-批量删除")
	@ApiOperation(value="百问百答-批量删除", notes="百问百答-批量删除")
	//@RequiresPermissions("org.jeecg.modules.demo:crawl_practical_details_discuss:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.crawlPracticalDetailsDiscussService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "百问百答-通过id查询")
	@ApiOperation(value="百问百答-通过id查询", notes="百问百答-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<CrawlPracticalDetailsDiscuss> queryById(@RequestParam(name="id",required=true) String id) {
		CrawlPracticalDetailsDiscuss crawlPracticalDetailsDiscuss = crawlPracticalDetailsDiscussService.getById(id);
		if(crawlPracticalDetailsDiscuss==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(crawlPracticalDetailsDiscuss);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param crawlPracticalDetailsDiscuss
    */
    //@RequiresPermissions("org.jeecg.modules.demo:crawl_practical_details_discuss:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CrawlPracticalDetailsDiscuss crawlPracticalDetailsDiscuss) {
        return super.exportXls(request, crawlPracticalDetailsDiscuss, CrawlPracticalDetailsDiscuss.class, "百问百答");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    //@RequiresPermissions("crawl_practical_details_discuss:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, CrawlPracticalDetailsDiscuss.class);
    }

}
