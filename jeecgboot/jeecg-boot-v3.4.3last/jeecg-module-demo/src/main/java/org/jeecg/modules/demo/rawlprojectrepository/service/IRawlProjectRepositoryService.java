package org.jeecg.modules.demo.rawlprojectrepository.service;

import org.jeecg.modules.demo.rawlprojectrepository.entity.RawlProjectRepository;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 项目库列表
 * @Author: jeecg-boot
 * @Date:   2023-08-12
 * @Version: V1.0
 */
public interface IRawlProjectRepositoryService extends IService<RawlProjectRepository> {

}
