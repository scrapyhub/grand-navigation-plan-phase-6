package org.jeecg.modules.demo.crawlpracticalexercisesactivity.service.impl;

import org.jeecg.modules.demo.crawlpracticalexercisesactivity.entity.CrawlPracticalExercisesActivity;
import org.jeecg.modules.demo.crawlpracticalexercisesactivity.mapper.CrawlPracticalExercisesActivityMapper;
import org.jeecg.modules.demo.crawlpracticalexercisesactivity.service.ICrawlPracticalExercisesActivityService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 往期实战
 * @Author: jeecg-boot
 * @Date:   2023-08-07
 * @Version: V1.0
 */
@Service
public class CrawlPracticalExercisesActivityServiceImpl extends ServiceImpl<CrawlPracticalExercisesActivityMapper, CrawlPracticalExercisesActivity> implements ICrawlPracticalExercisesActivityService {

}
