package org.jeecg.modules.demo.crawlhomepagequeryarticle.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.demo.crawlhomepagequeryarticle.entity.CrawlHomePageQueryArticle;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 最新主题列表
 * @Author: jeecg-boot
 * @Date:   2023-09-02
 * @Version: V1.0
 */
public interface CrawlHomePageQueryArticleMapper extends BaseMapper<CrawlHomePageQueryArticle> {

}
