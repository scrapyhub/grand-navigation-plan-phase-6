package org.jeecg.modules.demo.crawlhomepagequeryarticle.entity;

import lombok.Data;

import java.util.Map;
import java.util.List;
@Data
public class CrawlHomePageQueryArticleVO extends CrawlHomePageQueryArticleBasicVO {

    /**组ID*/
    private Map<String,Object> groupInfo;

    /**
     * 发布者信息
     */
    private Map<String,Object> talkOwner;

    /**
     * 图片内容
     * */
    private List<Map<String,Object>> talkImages;

    /**最近喜欢*/
    private List<Map<String,Object>> latestLikes;

    /** 显示评论 */
    private List<Map<String, Object>> showComments;

    /**
     * 用户特定信息
     */
    private Map<String,Object> userSpecific;

    /** tags */
    private List<Map<String, Object>> hashtags;

    /**
     * 附件列表
     * */
    private List<Map<String,Object>> talkFiles;

}
