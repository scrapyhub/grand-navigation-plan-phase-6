package org.jeecg.modules.demo.rawlprojectrepository.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 项目库列表
 * @Author: jeecg-boot
 * @Date:   2023-08-12
 * @Version: V1.0
 */
@Data
@TableName("rawl_project_repository")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="rawl_project_repository对象", description="项目库列表")
public class RawlProjectRepository implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private java.lang.Integer id;
	/**内容数量*/
	@Excel(name = "内容数量", width = 15)
    @ApiModelProperty(value = "内容数量")
    private java.lang.String contentCount;
	/**热门*/
	@Excel(name = "热门", width = 15)
    @ApiModelProperty(value = "热门")
    private java.lang.String hot;
	/**热门关键词*/
	@Excel(name = "热门关键词", width = 15)
    @ApiModelProperty(value = "热门关键词")
    private java.lang.String hotWords;
	/**项目*/
	@Excel(name = "项目", width = 15)
    @ApiModelProperty(value = "项目")
    private java.lang.String project;
	/**工具*/
	@Excel(name = "工具", width = 15)
    @ApiModelProperty(value = "工具")
    private java.lang.String toolbar;
	/**主题*/
	@Excel(name = "主题", width = 15)
    @ApiModelProperty(value = "主题")
    private java.lang.String topic;
}
