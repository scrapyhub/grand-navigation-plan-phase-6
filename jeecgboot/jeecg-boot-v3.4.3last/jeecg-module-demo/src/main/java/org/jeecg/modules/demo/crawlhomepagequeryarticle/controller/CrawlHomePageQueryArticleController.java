package org.jeecg.modules.demo.crawlhomepagequeryarticle.controller;

import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.isoops.slib.utils.SBeanUtil;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.demo.crawlhomepagequeryarticle.entity.CrawlHomePageQueryArticle;
import org.jeecg.modules.demo.crawlhomepagequeryarticle.entity.CrawlHomePageQueryArticleVO;
import org.jeecg.modules.demo.crawlhomepagequeryarticle.service.ICrawlHomePageQueryArticleService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 最新主题列表
 * @Author: jeecg-boot
 * @Date:   2023-09-02
 * @Version: V1.0
 */
@Api(tags="最新主题列表")
@RestController
@RequestMapping("/crawlhomepagequeryarticle/crawlHomePageQueryArticle")
@Slf4j
public class CrawlHomePageQueryArticleController extends JeecgController<CrawlHomePageQueryArticle, ICrawlHomePageQueryArticleService> {
	@Autowired
	private ICrawlHomePageQueryArticleService crawlHomePageQueryArticleService;
	
	/**
	 * 分页列表查询
	 *
	 * @param crawlHomePageQueryArticle
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "最新主题列表-分页列表查询")
	@ApiOperation(value="最新主题列表-分页列表查询", notes="最新主题列表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<CrawlHomePageQueryArticleVO>> queryPageList(CrawlHomePageQueryArticle crawlHomePageQueryArticle,
																	@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
																	@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
																	HttpServletRequest req) {

		QueryWrapper<CrawlHomePageQueryArticle> queryWrapper = QueryGenerator.initQueryWrapper(crawlHomePageQueryArticle, req.getParameterMap());
		Page<CrawlHomePageQueryArticle> page = new Page<CrawlHomePageQueryArticle>(pageNo, pageSize);
		IPage<CrawlHomePageQueryArticle> pageList = crawlHomePageQueryArticleService.page(page, queryWrapper);

		List<CrawlHomePageQueryArticleVO> result = new ArrayList<>();
		IPage<CrawlHomePageQueryArticleVO> iPage = new Page<>();
		iPage.setSize(pageList.getSize());
		iPage.setPages(pageList.getPages());
		iPage.setTotal(pageList.getTotal());
		iPage.setCurrent(pageList.getCurrent());

		List<CrawlHomePageQueryArticle> list = pageList.getRecords();
		for (CrawlHomePageQueryArticle article : list) {
			CrawlHomePageQueryArticleVO vo = SBeanUtil.clone(article,CrawlHomePageQueryArticleVO.class);
			vo.setGroupInfo((Map<String, Object>) JSON.parse(article.getGroupInfo()));
			vo.setTalkOwner((Map<String, Object>) JSON.parse(article.getTalkOwner()));
			vo.setUserSpecific((Map<String, Object>) JSON.parse(article.getUserSpecific()));
			List arrayList  = JSON.parseArray(article.getLatestLikes());
			vo.setLatestLikes(arrayList);
			List talkImages = JSON.parseArray(article.getTalkImages());
			vo.setTalkImages(talkImages);
			List showComments = JSON.parseArray(article.getShowComments());
			vo.setShowComments(showComments);
			List tags = JSON.parseArray(article.getHashtags());
			vo.setHashtags(tags);
			List talkFiles = JSON.parseArray(article.getTalkFiles());
			vo.setTalkFiles(talkFiles);
//			vo.setLatestLikes(JSON.parseArray(article.getLatestLikes()));
//			vo.setTalkImages(JSON.parseArray(article.getTalkImages()));
//			vo.setShowComments(JSON.parseArray(article.getShowComments()));
//			vo.setHashtags(JSON.parseArray(article.getHashtags()));
//			vo.setTalkFiles(JSON.parseArray(article.getTalkFiles()));

			result.add(vo);
		}

		iPage.setRecords(result);

		return Result.OK(iPage);
	}
	
	/**
	 *   添加
	 *
	 * @param crawlHomePageQueryArticle
	 * @return
	 */
	@AutoLog(value = "最新主题列表-添加")
	@ApiOperation(value="最新主题列表-添加", notes="最新主题列表-添加")
	//@RequiresPermissions("org.jeecg.modules.demo:crawl_home_page_query_article:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody CrawlHomePageQueryArticle crawlHomePageQueryArticle) {
		crawlHomePageQueryArticleService.save(crawlHomePageQueryArticle);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param crawlHomePageQueryArticle
	 * @return
	 */
	@AutoLog(value = "最新主题列表-编辑")
	@ApiOperation(value="最新主题列表-编辑", notes="最新主题列表-编辑")
	//@RequiresPermissions("org.jeecg.modules.demo:crawl_home_page_query_article:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody CrawlHomePageQueryArticle crawlHomePageQueryArticle) {
		crawlHomePageQueryArticleService.updateById(crawlHomePageQueryArticle);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "最新主题列表-通过id删除")
	@ApiOperation(value="最新主题列表-通过id删除", notes="最新主题列表-通过id删除")
	//@RequiresPermissions("org.jeecg.modules.demo:crawl_home_page_query_article:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		crawlHomePageQueryArticleService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "最新主题列表-批量删除")
	@ApiOperation(value="最新主题列表-批量删除", notes="最新主题列表-批量删除")
	//@RequiresPermissions("org.jeecg.modules.demo:crawl_home_page_query_article:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.crawlHomePageQueryArticleService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "最新主题列表-通过id查询")
	@ApiOperation(value="最新主题列表-通过id查询", notes="最新主题列表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<CrawlHomePageQueryArticle> queryById(@RequestParam(name="id",required=true) String id) {
		CrawlHomePageQueryArticle crawlHomePageQueryArticle = crawlHomePageQueryArticleService.getById(id);
		if(crawlHomePageQueryArticle==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(crawlHomePageQueryArticle);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param crawlHomePageQueryArticle
    */
    //@RequiresPermissions("org.jeecg.modules.demo:crawl_home_page_query_article:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CrawlHomePageQueryArticle crawlHomePageQueryArticle) {
        return super.exportXls(request, crawlHomePageQueryArticle, CrawlHomePageQueryArticle.class, "最新主题列表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    //@RequiresPermissions("crawl_home_page_query_article:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, CrawlHomePageQueryArticle.class);
    }

}
