package org.jeecg.modules.demo.crawlhomepagequeryarticle.service;

import org.jeecg.modules.demo.crawlhomepagequeryarticle.entity.CrawlHomePageQueryArticle;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 最新主题列表
 * @Author: jeecg-boot
 * @Date:   2023-09-02
 * @Version: V1.0
 */
public interface ICrawlHomePageQueryArticleService extends IService<CrawlHomePageQueryArticle> {

}
