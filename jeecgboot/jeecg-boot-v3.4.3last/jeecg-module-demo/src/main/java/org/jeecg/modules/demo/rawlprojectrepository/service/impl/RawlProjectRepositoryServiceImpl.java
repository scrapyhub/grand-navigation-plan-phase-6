package org.jeecg.modules.demo.rawlprojectrepository.service.impl;

import org.jeecg.modules.demo.rawlprojectrepository.entity.RawlProjectRepository;
import org.jeecg.modules.demo.rawlprojectrepository.mapper.RawlProjectRepositoryMapper;
import org.jeecg.modules.demo.rawlprojectrepository.service.IRawlProjectRepositoryService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 项目库列表
 * @Author: jeecg-boot
 * @Date:   2023-08-12
 * @Version: V1.0
 */
@Service
public class RawlProjectRepositoryServiceImpl extends ServiceImpl<RawlProjectRepositoryMapper, RawlProjectRepository> implements IRawlProjectRepositoryService {

}
