package org.jeecg.modules.demo.crawlpracticaldetailsVeteran.service.impl;

import org.jeecg.modules.demo.crawlpracticaldetailsVeteran.entity.CrawlPracticalDetailsVeteran;
import org.jeecg.modules.demo.crawlpracticaldetailsVeteran.mapper.CrawlPracticalDetailsVeteranMapper;
import org.jeecg.modules.demo.crawlpracticaldetailsVeteran.service.ICrawlPracticalDetailsVeteranService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 高手分享
 * @Author: jeecg-boot
 * @Date:   2023-08-07
 * @Version: V1.0
 */
@Service
public class CrawlPracticalDetailsVeteranServiceImpl extends ServiceImpl<CrawlPracticalDetailsVeteranMapper, CrawlPracticalDetailsVeteran> implements ICrawlPracticalDetailsVeteranService {

}
