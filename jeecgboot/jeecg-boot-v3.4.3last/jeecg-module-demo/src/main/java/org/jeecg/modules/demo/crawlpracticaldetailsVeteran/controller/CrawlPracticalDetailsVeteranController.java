package org.jeecg.modules.demo.crawlpracticaldetailsVeteran.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.crawlpracticaldetailsVeteran.entity.CrawlPracticalDetailsVeteran;
import org.jeecg.modules.demo.crawlpracticaldetailsVeteran.service.ICrawlPracticalDetailsVeteranService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 高手分享
 * @Author: jeecg-boot
 * @Date:   2023-08-07
 * @Version: V1.0
 */
@Api(tags="高手分享")
@RestController
@RequestMapping("/crawlpracticaldetailsVeteran/crawlPracticalDetailsVeteran")
@Slf4j
public class CrawlPracticalDetailsVeteranController extends JeecgController<CrawlPracticalDetailsVeteran, ICrawlPracticalDetailsVeteranService> {
	@Autowired
	private ICrawlPracticalDetailsVeteranService crawlPracticalDetailsVeteranService;
	
	/**
	 * 分页列表查询
	 *
	 * @param crawlPracticalDetailsVeteran
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "高手分享-分页列表查询")
	@ApiOperation(value="高手分享-分页列表查询", notes="高手分享-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<CrawlPracticalDetailsVeteran>> queryPageList(CrawlPracticalDetailsVeteran crawlPracticalDetailsVeteran,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<CrawlPracticalDetailsVeteran> queryWrapper = QueryGenerator.initQueryWrapper(crawlPracticalDetailsVeteran, req.getParameterMap());
		Page<CrawlPracticalDetailsVeteran> page = new Page<CrawlPracticalDetailsVeteran>(pageNo, pageSize);
		IPage<CrawlPracticalDetailsVeteran> pageList = crawlPracticalDetailsVeteranService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param crawlPracticalDetailsVeteran
	 * @return
	 */
	@AutoLog(value = "高手分享-添加")
	@ApiOperation(value="高手分享-添加", notes="高手分享-添加")
	//@RequiresPermissions("org.jeecg.modules.demo:crawl_practical_details_veteran:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody CrawlPracticalDetailsVeteran crawlPracticalDetailsVeteran) {
		crawlPracticalDetailsVeteranService.save(crawlPracticalDetailsVeteran);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param crawlPracticalDetailsVeteran
	 * @return
	 */
	@AutoLog(value = "高手分享-编辑")
	@ApiOperation(value="高手分享-编辑", notes="高手分享-编辑")
	//@RequiresPermissions("org.jeecg.modules.demo:crawl_practical_details_veteran:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody CrawlPracticalDetailsVeteran crawlPracticalDetailsVeteran) {
		crawlPracticalDetailsVeteranService.updateById(crawlPracticalDetailsVeteran);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "高手分享-通过id删除")
	@ApiOperation(value="高手分享-通过id删除", notes="高手分享-通过id删除")
	//@RequiresPermissions("org.jeecg.modules.demo:crawl_practical_details_veteran:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		crawlPracticalDetailsVeteranService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "高手分享-批量删除")
	@ApiOperation(value="高手分享-批量删除", notes="高手分享-批量删除")
	//@RequiresPermissions("org.jeecg.modules.demo:crawl_practical_details_veteran:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.crawlPracticalDetailsVeteranService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "高手分享-通过id查询")
	@ApiOperation(value="高手分享-通过id查询", notes="高手分享-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<CrawlPracticalDetailsVeteran> queryById(@RequestParam(name="id",required=true) String id) {
		CrawlPracticalDetailsVeteran crawlPracticalDetailsVeteran = crawlPracticalDetailsVeteranService.getById(id);
		if(crawlPracticalDetailsVeteran==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(crawlPracticalDetailsVeteran);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param crawlPracticalDetailsVeteran
    */
    //@RequiresPermissions("org.jeecg.modules.demo:crawl_practical_details_veteran:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CrawlPracticalDetailsVeteran crawlPracticalDetailsVeteran) {
        return super.exportXls(request, crawlPracticalDetailsVeteran, CrawlPracticalDetailsVeteran.class, "高手分享");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    //@RequiresPermissions("crawl_practical_details_veteran:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, CrawlPracticalDetailsVeteran.class);
    }

}
