package org.jeecg.modules.demo.topicqueryarticle.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 主题详情页
 * @Author: jeecg-boot
 * @Date:   2023-08-03
 * @Version: V1.0
 */
@Data
@TableName("crawl_topic_query_article")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="crawl_topic_query_article对象", description="主题详情页")
public class TopicQueryArticle implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private java.lang.Integer id;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
	/**主题ID*/
	@Excel(name = "主题ID", width = 15)
    @ApiModelProperty(value = "主题ID")
    private java.lang.String topicId;
	/**组ID*/
	@Excel(name = "组ID", width = 15)
    @ApiModelProperty(value = "组ID")
    private java.lang.Integer groupId;
	/**组名称*/
	@Excel(name = "组名称", width = 15)
    @ApiModelProperty(value = "组名称")
    private java.lang.String groupName;
	/**组类型*/
	@Excel(name = "组类型", width = 15)
    @ApiModelProperty(value = "组类型")
    private java.lang.String groupType;
	/**组背景图片url*/
	@Excel(name = "组背景图片url", width = 15)
    @ApiModelProperty(value = "组背景图片url")
    private java.lang.String groupBackgroundUrl;
	/**内容类型*/
	@Excel(name = "内容类型", width = 15)
    @ApiModelProperty(value = "内容类型")
    private java.lang.String contentType;
	/**文本内容*/
	@Excel(name = "文本内容", width = 15)
    @ApiModelProperty(value = "文本内容")
    private java.lang.String content;
	/**发布者用户ID*/
	@Excel(name = "发布者用户ID", width = 15)
    @ApiModelProperty(value = "发布者用户ID")
    private java.math.BigDecimal ownerUserId;
	/**发布者姓名*/
	@Excel(name = "发布者姓名", width = 15)
    @ApiModelProperty(value = "发布者姓名")
    private java.lang.String ownerName;
	/**发布者别名*/
	@Excel(name = "发布者别名", width = 15)
    @ApiModelProperty(value = "发布者别名")
    private java.lang.String ownerAlias;
	/**发布者头像URL*/
	@Excel(name = "发布者头像URL", width = 15)
    @ApiModelProperty(value = "发布者头像URL")
    private java.lang.String ownerAvatarUrl;
	/**发布者所在地*/
	@Excel(name = "发布者所在地", width = 15)
    @ApiModelProperty(value = "发布者所在地")
    private java.lang.String ownerLocation;
	/**最新点赞用户信息*/
	@Excel(name = "最新点赞用户信息", width = 15)
    @ApiModelProperty(value = "最新点赞用户信息")
    private java.lang.String latestLikes;
	/**点赞数*/
	@Excel(name = "点赞数", width = 15)
    @ApiModelProperty(value = "点赞数")
    private java.lang.Integer likesCount;
	/**赞赏数*/
	@Excel(name = "赞赏数", width = 15)
    @ApiModelProperty(value = "赞赏数")
    private java.lang.Integer rewardsCount;
	/**评论数*/
	@Excel(name = "评论数", width = 15)
    @ApiModelProperty(value = "评论数")
    private java.lang.Integer commentsCount;
	/**是否加精*/
	@Excel(name = "是否加精", width = 15)
    @ApiModelProperty(value = "是否加精")
    private java.lang.Integer contentDigested;
	/**是否置顶*/
	@Excel(name = "是否置顶", width = 15)
    @ApiModelProperty(value = "是否置顶")
    private java.lang.Integer sticky;
	/**内容发布时间*/
	@Excel(name = "内容发布时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "内容发布时间")
    private java.util.Date contentCreateTime;
	/**内容修改时间*/
	@Excel(name = "内容修改时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "内容修改时间")
    private java.util.Date contentModifyTime;
	/**当前用户是否点赞*/
	@Excel(name = "当前用户是否点赞", width = 15)
    @ApiModelProperty(value = "当前用户是否点赞")
    private java.lang.String userSpecificLiked;
	/**是否加精*/
	@Excel(name = "是否加精", width = 15)
    @ApiModelProperty(value = "是否加精")
    private java.lang.Integer digested;
	/**用户ID*/
	@Excel(name = "用户ID", width = 15)
    @ApiModelProperty(value = "用户ID")
    private java.lang.String uid;
	/**对象ID*/
	@Excel(name = "对象ID", width = 15)
    @ApiModelProperty(value = "对象ID")
    private java.lang.String objectid;
	/**对象类型*/
	@Excel(name = "对象类型", width = 15)
    @ApiModelProperty(value = "对象类型")
    private java.lang.String classname;
	/**标签*/
	@Excel(name = "标签", width = 15)
    @ApiModelProperty(value = "标签")
    private java.lang.String hashtags;
}
