package org.jeecg.modules.demo.rawlprojectrepository.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.demo.rawlprojectrepository.entity.RawlProjectRepository;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 项目库列表
 * @Author: jeecg-boot
 * @Date:   2023-08-12
 * @Version: V1.0
 */
public interface RawlProjectRepositoryMapper extends BaseMapper<RawlProjectRepository> {

}
