package org.jeecg.modules.system.vo;

import com.isoops.slib.pojo.AbstractObject;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Description: Token登录Vo
 * @Author: jeecg-boot
 * @Date: 2022-06-14
 * @Version: V1.0
 */

@ApiModel(value = "Token登录Vo", description = "Token登录Vo")
@EqualsAndHashCode(callSuper = false)
@Data
public class TokenLoginVO extends AbstractObject {

    /** 用户认证凭据 */
    private String token;
    /** 过期时间 */
    //通常是总毫秒数: token生成时间+有效时长
    private long expTime;
    /** 生成时间 */
    //通常是总毫秒数
    private long genTime;

}