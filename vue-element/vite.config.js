import { defineConfig,loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import Components from 'unplugin-vue-components/vite'
import { VantResolver } from 'unplugin-vue-components/resolvers';
import path from 'path'
import VitePluginWindicss from "vite-plugin-windicss";
function resolve(dir) {
  return path.join(__dirname, dir)
}


export default (({mode})=>{
    const env=loadEnv(mode, process.cwd()) // 获取.env文件里定义的环境变量
    return defineConfig({
        plugins: [
            vue(),
            Components(
                {
                    resolvers: [VantResolver()]
                }
            ),
            VitePluginWindicss()
        ],
        resolve: {
            alias: {
                '@': resolve('src'),
            }
        },
        server:{

        }
    })
})
