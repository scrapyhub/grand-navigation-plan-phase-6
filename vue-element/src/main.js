import { createApp } from 'vue'
import 'virtual:windi.css'
import 'vant/lib/index.css'//引入样式
import './style.css'
import shareCard from './components/shareCard.vue'
import ellipsis from './components/ellipsis.vue'
import App from './App.vue'
const app = createApp(App)
import router from "./modules/router.js";
app.component('share-card', shareCard);
app.component('ellipsis', ellipsis);
app.use(router)
app.mount('#app')

