import request from "@/utils/request.js";
import axios from "axios";


export function tokenLogin (token) {
    const url = import.meta.env.VITE_BASE_URL
    return axios({
        url: `${url}/jeecg-boot/sys/token/tokenLogin`,
        method: 'get',
        headers: {
            'Content-Type': 'application/json',
            'X-Access-Token': token
        }

    })
};


//  更新token
export const tokenRefresh = query => {
    return request({
        url: '/jeecg-boot/sys/token/tokenRefresh',
        method: 'get',
        params: {
            paramsToken:query
        }
    });
};
