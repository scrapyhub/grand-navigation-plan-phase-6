import request from '../utils/request';

// 最新主题列表-分页列表查询
export const homePageQueryArticle = query => {
    return request({
        url: '/jeecg-boot/crawlhomepagequeryarticle/crawlHomePageQueryArticle/list',
        method: 'get',
        params: query
    });
};


//  往期实战-分页列表查询
export const crawlPracticalExercisesActivity = query => {
    return request({
        url: '/jeecg-boot/crawlpracticalexercisesactivity/crawlPracticalExercisesActivity/list',
        method: 'get',
        params: query
    });
};

export const topicQueryArticleQueryById = query => {
    return request({
        url: '/jeecg-boot/crawlhomepagequeryarticle/crawlHomePageQueryArticle/queryById?id=' + query,
        method: 'get',
    });
};
