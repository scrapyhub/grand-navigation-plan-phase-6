import {getLocalStorage, removeLocalStorage, setLocalStorage} from "@/utils/localStorage.js";

const TOKEN_KEY = "x-h6-token"
const RESET_TOKEN_KEY = "x-h6-reset-token"

export function getToken(){
    return getLocalStorage(TOKEN_KEY)
}

export function setToken(value){

    return setLocalStorage(TOKEN_KEY,value)
}

export function removeToken(){
    return removeLocalStorage(TOKEN_KEY)
}

export function getResetToken(){
    return getLocalStorage(RESET_TOKEN_KEY)
}

export function setResetToken(value){
    return setLocalStorage(RESET_TOKEN_KEY,value)
}


export function removeResetToken(){
    return removeLocalStorage(RESET_TOKEN_KEY)
}
