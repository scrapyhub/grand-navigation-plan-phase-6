import {createRouter, createWebHistory} from "vue-router"

const router = createRouter({
    routes: [{
        name: 'home',
        path: '/',
        component: () => import('@/pages/home.vue')
    },{
        name: 'latestActualCombat',
        path: '/latestActualCombat',
        component: () => import('../pages/latestActualCombat.vue')
    },{
        name: 'activity',
        path: '/activity',
        component: () => import('../pages/activity.vue')
    },{
        name: 'pastCombat',
        path: '/pastCombat',
        component: () => import('../pages/pastCombat.vue')
    },{
        name: 'focus',
        path: '/focus',
        component: () => import('../pages/focus.vue')
    },{
        name: 'collect',
        path: '/collect',
        component: () => import('../pages/collect.vue')
    },{
        name: 'history',
        path: '/history',
        component: () => import('../pages/history.vue')
    },{
        name: 'login',
        path: '/login',
        component: () => import('../pages/login.vue')
    },{
        name: 'detail',
        path: '/detail',
        component: () => import('../pages/detailShareCard.vue')
    },{
        name: 'userPage',
        path: '/userPage',
        component: () => import('../pages/userPage.vue')
    },{
        name: 'tagPage',
        path: '/tagPage',
        component: () => import('../pages/tagPage.vue')
    },{
        name: 'search',
        path: '/search',
        component: () => import('../pages/search.vue')
    }],
    history: createWebHistory()
})
export default router
